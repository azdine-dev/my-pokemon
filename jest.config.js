module.exports = {
    // The root directory that Jest should scan for tests.
    roots: ['<rootDir>/src'],
  
    // A list of file extensions that Jest should look for.
    testMatch: ['**/__tests__/**/*.test.tsx'],
  
    // Transform files with TypeScript using ts-jest.
    transform: {
      '^.+\\.tsx?$': 'ts-jest',
    },
  
    // The module file extensions for which mock modules should be automatically created.
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  
    // Additional configuration options for Jest.
    // You can add more options here as needed.
    // For example, you might want to set up coverage reports, etc.
  };