import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import PokedexContainer from '../PokedexContainer';
import rootReducer from '../../../redux/reducers';
import initState from '../../../redux/reducers/initial-state';
import '@testing-library/jest-dom'


// Create the mock store using configureStore
const store = configureStore({
  reducer: rootReducer,
  preloadedState: initState,
});

test('renders PokedexContainer component', () => {
  // Use render from @testing-library/react to render the component with the mock store
  render(
    <Provider store={store}>
      <PokedexContainer pokemons={initState.pokemons}
                        ui={{searchPayload : ''}}
                        selectedPokemon={initState.selectedPokemon} />
    </Provider>
  );

  // Use screen.getByText to get the element by its text content
  const headerElement = screen.getByText('Your Ultimapte website to view your prefered Pokemons');
  expect(headerElement).toBeInTheDocument();
});
