export const availablePokeTypes :string[] = [
    'type-normal', 'type-flying','type-ghost','type-dark', 
    'type-steel','type-ground', 'type-poison', 'type-grass', 
    'type-fire', 'type-electric', 'type-fairy','type-bug',
    'type-fighting','type-water', 'type-psychic', 'type-ice',
     'type-rock', 'type-dragon',     
  ];
    